var APPID = "8a98215e56f708268a2190081695e9b6";
var temp;
var loc;
var icon;
var humidity;
var wind;
var direction;

function updateByZip(zip) {
    //https://cors-anywhere.herokuapp.com/ becouse https
    var url = "https://cors-anywhere.herokuapp.com/http://api.openweathermap.org/data/2.5/weather?" + "zip=" + zip + "&APPID=" + APPID;
	console.log("UBZ: " + url);
    sendRequest(url);
}

function sendRequest(url){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
			var data = JSON.parse(xmlhttp.responseText);
			var weather = {};
			weather.code = data.weather[0].id;
			weather.humidity = data.main.humidity;
			weather.wind = data.wind.speed;
			weather.direction = degreesToDirection(data.wind.deg);
			weather.loc = data.name;
			weather.temp = K2C(data.main.temp);		
			update(weather);
		}
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();    
}

$(document).ready(function () {

        $.ajax({
            type: 'POST',
            url: 'http://redrock.com:6606/api/values/get',

            dataType: "jsonp",

            crossDomain: true,
            success: function (msg) {

                alert("success");

            },
            error: function (request, status, error) {

                alert(error);
            }
        });
    });





function degreesToDirection(degrees) {
    var range = 360/16;
    var low = 360 - range/2;
    var high = (low + range) % 360;
    var angles = ["N", "NNE", "NE", "ENE", "E", "ESE", "SE", "SSE", "S", "SSW", "SW", "WSW", "W", "WNW", "NW", "NNW"];
    for( var i in angles ) {
	if( degrees >= low && degrees < high ) {
	    //console.log(angles[i]);
	    return angles[i];
	    //console.log("derp");
	}
	low = (low + range) % 360;
	high = (high + range) % 360;
    }
    return "N";
}

/* From Kelvin to Celsium */
function K2C(k){
    return Math.round(k - 273.15);
}

function update(weather) {
    icon.src = "imgs/codes/" + weather.code + ".png"
    humidity.innerHTML = weather.humidity;
    wind.innerHtml = weather.wind;
    direction.innerHTML = weather.direction;
    loc.innerHTML = weather.location;
    temp.innerHTML = weather.temp;
}

window.onload = function () {
    temp = document.getElementById("temperature");
    loc = document.getElementById("location");
    icon = document.getElementById("icon");
    humidity = document.getElementById("humidity");
    wind = document.getElementById("wind");
    direction = document.getElementById("direction");
	// 3196359 - Ljubljana
	updateByZip(3196359);
}