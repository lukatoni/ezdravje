
var baseUrl = 'https://rest.ehrscape.com/rest/v1';

var username = "ois.seminar";
var password = "ois4fri";

var sessionID;

var ehrIdList = [];

var googleMap;
var activeMarkers = [];

function getSessionId () {
    var response = $.ajax({
        type: "POST",
        url: baseUrl + "/session?username=" + encodeURIComponent(username) +
                "&password=" + encodeURIComponent(password),
        async: false
    });
    return response.responseJSON.sessionId;
}


//region DISPLAY FUNCTIONS
function displayData(ehrId) {
    // Display the data
    var callback = function (retrievedData) {
        if (retrievedData) {
            $("#name").html(retrievedData.firstNames + " " + retrievedData.lastNames);
            var age = retrievedData.partyAdditionalInfo.filter(function(obj) {return obj.key === "age";})[0].value;
            $("#age").html(age);
            var sex = retrievedData.partyAdditionalInfo.filter(function(obj) {return obj.key === "sex";})[0].value;
            $("#sex").html(sex);
            var nationality = retrievedData.partyAdditionalInfo.filter(function(obj) {return obj.key === "nationality";})[0].value;
            $("#nationality").html(nationality);
            var height = retrievedData.partyAdditionalInfo.filter(function(obj) {return obj.key === "height";})[0].value;
            $("#height").html(height + "cm");

            var weightMeasurements = destringifyGraphData(retrievedData.partyAdditionalInfo.filter(function(obj) {return obj.key === "weightMeasurements";})[0].value);
            plotWeight(weightMeasurements);
            
            var injuries = destringifyInjuries(retrievedData.partyAdditionalInfo.filter(function (obj) { return obj.key === "injuries";})[0].value);
            displayInjuries(injuries);
        }
    };

    fetchEhrSubjectData(ehrId, callback);
}

function displayInjuries(injuries) {
    var injuriesList = document.getElementById("injuriesList");
    injuriesList.innerHTML = "";

    for (var i = 0; i < activeMarkers.length; i++) {
        activeMarkers[i].setMap(null);
    }

    activeMarkers = [];

    for (var i = 0; i < injuries.length; i++) {
        var entry =
            "<div class='panel panel-default'>\
                <div class='panel-heading'> \
                    <h4 class='panel-title'>\
                        <a data-toggle='collapse' href='#collapse" + i +"'>" + injuries[i].type + "</a>\
                    </h4>\
                </div>\
                <div id='collapse" + i + "' class='panel-collapse collapse'>\
                    <div class='panel-body'>\
                        <div class='row'>\
                            <div class='col-md-3'>\
                                <b>Lokacija:</b> " + injuries[i].location + "\
                            </div>\
                            <div class='col-md-2'>\
                                <b>Datum:</b> " + injuries[i].timestamp + "\
                            </div>\
                            <div class='col-md-3'>\
                                <b>Terapija:</b> " + injuries[i].therapy + "\
                            </div>\
                            <div class='col-md-4'>\
                                <b>Čas okrevanja:</b> " + injuries[i].recoveryTime + " dni  " + "\
                            </div>\
                        </div>\
                    </div>\
                </div>\
            </div>"

        injuriesList.innerHTML += entry;

        // Create google map markers
        var marker = new google.maps.Marker({
            position: {lat: injuries[i].lat, lng:injuries[i].lng},
            map: googleMap
        });

        activeMarkers.push(marker);

        var infowindow = new google.maps.InfoWindow({
            content: injuries[i].type
        });

        infowindow.open(googleMap, marker);

    }
}
//endregion


//region EHR ACCESS
function addEhrSubject(subjectData, callback) {
    // Check if the session ID was already retrieved.. If not retrieve it.
    if (!sessionID) {
        sessionID = getSessionId();
    }

    $.ajaxSetup({
        headers: {"Ehr-Session": sessionID}
    });
    $.ajax({
        url: baseUrl + "/ehr",
        type: 'POST',
        success: function (data) {
            var ehrId = data.ehrId;
            // Hacks.. Hacks everywhere !!!  (╯°□°）╯︵ ┻━┻
            var partyData = {
                firstNames: subjectData.firstName,
                lastNames: subjectData.lastName,

                partyAdditionalInfo: [
                    {key: "ehrId", value: ehrId},
                    {key: "age", value: subjectData.age},
                    {key: "sex", value: subjectData.sex},
                    {key: "nationality", value: subjectData.nationality},
                    {key: "height", value: subjectData.height},
                    {key: "weightMeasurements", value: stringifyGraphData(subjectData.weightMeasurements)},
                    {key: "injuries", value: stringifyInjiries(subjectData.injuries)}
                ]
            };

            $.ajax({
                url: baseUrl + "/demographics/party",
                type: 'POST',
                contentType: 'application/json',
                data: JSON.stringify(partyData),

                success: function (party) {
                    if (party.action == 'CREATE') {
                        callback(ehrId, subjectData.firstName, subjectData.lastName);
                        console.log("Successfully created a new entry.")
                    }
                },
                error: function(err) {
                    callback();
                    console.error("Something went wrong while creating a new entry.");
                }
            });
        }
    });
}

function fetchEhrSubjectData(ehrId, callback) {
    sessionID = getSessionId();

    $.ajax({
        url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
        type: 'GET',
        headers: {"Ehr-Session": sessionID},
        success: function (data) {
            callback(data.party)
        },
        error: function(err) {
            callback();
        }
    });
}
//endregion

//region STRINGIFY HELPER FUNCTIONS
function stringifyInjiries(input) {
    var stringified = "";

    for (var i = 0; i < input.length; i++) {
        stringified += input[i].type + "/" + input[i].timestamp + "/" + input[i].therapy + "/" + input[i].recoveryTime + "/" + input[i].recovered + "/" + input[i].location + "/" + input[i].lat + "/" + input[i].lng + "/";
    }

    return stringified;
}

// Stringify JS objects to fool the ehrscape
function stringifyGraphData(input) {
    var stringified = "";
    for (var i = 0; i < input.length; i++) {
        stringified += input[i].timestamp + "/" + input[i].value + "/";
    }

    return stringified;
}

function destringifyInjuries(input) {
    var splitted = input.split("/");
    var injuries = [];

    for (var i = 0; i < splitted.length - 8; i+= 8) {
        injuries.push({type: splitted[i], timestamp: splitted[i+1], therapy: splitted[i+2], recoveryTime: parseFloat(splitted[i+3]), recovered: "true" === splitted[i+4], location: splitted[i+5], lat: parseFloat(splitted[i+6]), lng: parseFloat(splitted[i+7])});
    }

    return injuries;
}

// Convert stringified JS graph objects back to JS objects
function destringifyGraphData(input) {
    var splitted = input.split("/");

    var graphData = [];
    for (var i = 0; i < splitted.length-2; i+=2) {
        graphData.push({timestamp: splitted[i], value: parseFloat(splitted[i+1])});
    }

    return graphData;
}
//endregion

function getRndInteger(min, max) {
    return (Math.random() * (max - min)) + min;
}

function getSex() {
    var rand = Math.random();
    if(rand >= 0.5) {
        return "Moški";
    } else {
        return "Ženska";
    }
}

function makeid() {
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    var text = possible.charAt(Math.floor(Math.random() * possible.length));
    return text;
}

function makeType() {
   var rand = Math.random();
    if(rand >= 0.7) {
        return "Službeno";
    } else if (rand <= 0.7 && rand >= 0.3) {
        return "Dopust";
    } else {
        return "Zdravnik";
    }
}

function makeTrue() {
   var rand = Math.random();
    if(rand >= 0.7) {
        return true;
    } else {
        return false;
    }
}

function makeDate() {
   var rand = Math.round(getRndInteger(0,10))
   return rand + ". 4. 2017"
}

//region DATA GENERATION
function generateData () {

    var data = {
            firstName: makeid() + ".",
            lastName: makeid() + ".",
            sex: getSex(),
            age: Math.round(getRndInteger(20,100)),
            nationality: "SLO",
            height: Math.round(getRndInteger(150,210)),

            weightMeasurements: [
                {timestamp: "2017-01-11", value: getRndInteger(50,150)},
                {timestamp: "2017-02-11", value: getRndInteger(50,150)},
                {timestamp: "2017-03-11", value: getRndInteger(50,150)},
                {timestamp: "2017-04-12", value: getRndInteger(50,150)},
                {timestamp: "2017-05-12", value: getRndInteger(50,150)}
            ],

            injuries: [
                {type: makeType(), timestamp: makeDate(), therapy: "počitek", recoveryTime: Math.round(getRndInteger(0,10)), recovered: makeTrue(), location: makeid() + makeid() + "." , lat: getRndInteger(-20,50), lng: getRndInteger(-20,60)},
            ]
        };

    if (data) {
        var callback = function(ehrId, firstName, lastName) {
            if (ehrId && firstName && lastName) {
                document.getElementById("playersDropdown").innerHTML += '<li><a href="#" id="' + ehrId + '" onclick="selectPerson(this)">' + firstName + ' ' + lastName + '</a></li>';

                ehrIdList.push(ehrId);
                if (ehrIdList.length === 1) {
                    //displayData(ehrIdList[0]);
                }
            }
        };
        addEhrSubject(data, callback);
    }
}
//endregion

//region CLICK LISTENERS
function generatorOnClick() {
    for (var i = 0 ; i < 3 ; ++i) {
        generateData();
    }
}

function selectPerson(obj) {
    document.getElementById("ehrIdHolder").value = obj.id;
    displayData(obj.id);
}

function selectEhr() {
    var ehrId = document.getElementById("ehrIdHolder").value;
    displayData(ehrId);
}
//endregion

function plotWeight (input) {
    var w = document.getElementById('weightPlot').offsetWidth;

    var xVal = [];
    var yVal = [];

    for (var i = 0; i < input.length; i++) {
        xVal.push(input[i].timestamp);
        yVal.push(input[i].value);
    }

    var data = [
        {
            x: xVal,
            y: yVal,
            type: 'scatter'
        }
    ];

    var layout = {
        title: "Teža",
        width: w,
        height: w,
        margin: {
            l: 50,
            r: 40,
            b: 50,
            t: 40
        },
        xaxis: {
            title: 'Datum',
            showgrid: false,
        },
        yaxis: {
            title: 'Teža (kg)',
        }
    };
    Plotly.newPlot('weightPlot', data, layout, {displayModeBar: false});
}

$(document).ready(function() {
    plotWeight([]);
});
//endregion

// Google map initialization
function initMap() {
    googleMap = new google.maps.Map(document.getElementById('map'), {
        zoom: 2,
        center: {lat: 48.0, lng: 15.0}
    });
}